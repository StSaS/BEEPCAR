package main

import (

	//"encoding/json"
	"github.com/mailru/easyjson/jlexer"
	//"fmt"
	//"encoding/json"
	//"fmt"
	"io"
	"io/ioutil"
	"os"
	//
	//"regexp"
	"bytes"
	"reflect"
	"strconv"
	"strings"
	"unsafe"
	// "log"
)

//easyjson:json
type Item struct {
	Data_browsers []string `json:"browsers"`
	Data_email    string   `json:"email"`
	//Data_company *json.RawMessage `json:"company"`
	//Data_country string	`json:"country"`
	//Data_job string	`json:"job"`
	Data_name string `json:"name"`
	//Data_phone string `json:"phone"`
}

func BytesToString(b []byte) string {
	bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	sh := reflect.StringHeader{bh.Data, bh.Len}
	return *(*string)(unsafe.Pointer(&sh))
}

func FastSearch(out io.Writer) {
	file, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}

	fileContents, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}

	err = file.Close()

	if err != nil {
		panic(err)
	}

	seenBrowsers := make([]string, 0)
	uniqueBrowsers := 0

	buf := bytes.Buffer{}

	buf.WriteString("found users:\n")
	out.Write(buf.Bytes())

	lines := bytes.Split(fileContents, []byte("\n"))

	for i, line := range lines {

		lexer := new(jlexer.Lexer)

		user := Item{}

		*lexer = jlexer.Lexer{Data: line}

		user.UnmarshalEasyJSON(lexer)

		if lexer.Error() != nil {
			panic(lexer.Error())
		}

		isAndroid := false
		isMSIE := false

		browsers := user.Data_browsers

		for _, browserRaw := range browsers {
			browser := browserRaw

			if ok := strings.Contains(browser, "Android"); ok == true {
				isAndroid = true
				notSeenBefore := true
				for _, item := range seenBrowsers {
					if item == browser {
						notSeenBefore = false
					}
				}
				if notSeenBefore {
					// log.Printf("SLOW New browser: %s, first seen: %s", browser, user["name"])
					seenBrowsers = append(seenBrowsers, browser)
					uniqueBrowsers++
				}
			}
		}

		for _, browserRaw := range browsers {
			browser := browserRaw
			/*if !ok {
				// log.Println("cant cast browser to string")
				continue
			}*/

			if ok := strings.Contains(browser, "MSIE"); ok == true {
				isMSIE = true
				notSeenBefore := true
				for _, item := range seenBrowsers {
					if item == browser {
						notSeenBefore = false
					}
				}
				if notSeenBefore {
					// log.Printf("SLOW New browser: %s, first seen: %s", browser, user["name"])
					seenBrowsers = append(seenBrowsers, browser)
					uniqueBrowsers++
				}
			}
		}

		if !(isAndroid && isMSIE) {
			continue
		}

		// log.Println("Android and MSIE user:", user["name"], user["email"])

		email := strings.Replace(user.Data_email, "@", " [at] ", -1)

		buf.Reset()

		buf.WriteByte('[')
		buf.WriteString(strconv.Itoa(i))
		buf.WriteString("] ")
		buf.WriteString(user.Data_name)
		buf.WriteString(" <")
		buf.WriteString(email)
		buf.WriteString(">\n")

		out.Write(buf.Bytes())

	}

	buf.Reset()

	buf.WriteString("\n")
	buf.WriteString("Total unique browsers ")
	buf.WriteString(strconv.Itoa(len(seenBrowsers)))
	buf.WriteString("\n")

	out.Write(buf.Bytes())

}

func main() {
	//FastSearch(ioutil.Discard)
}
