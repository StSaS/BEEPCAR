package main

import (
	"strconv"
	//"strconv"
	//"encoding/json"
	"context"
	"fmt"
	"gopkg.in/telegram-bot-api.v4"
	"net/http"
	"regexp"
)

var (
	// @BotFather gives you this
	BotToken    = "XXX"
	WebhookURL  = "https://525f2cb5.ngrok.io"
	counterTask = 0
	tasks       = make([]Task, 0)
)

type Task struct {
	id             int // номер задачи
	text           string
	assign         int64  // id исполнителя задачи (256)
	assignUserName string // username исполнителя (aivanov)
	owner          int64  // id создателя команды
	ownerUserName  string //
}

func startTaskBot(ctx context.Context) error {
	// сюда пишите ваш код

	bot, err := tgbotapi.NewBotAPI(BotToken)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Authorized on account %s\n", bot.Self.UserName)

	_, err = bot.SetWebhook(tgbotapi.NewWebhook(WebhookURL))
	if err != nil {
		panic(err)
	}

	updates := bot.ListenForWebhook("/")

	go http.ListenAndServe(":8081", nil)
	fmt.Println("start listen :8081")

	for {
		select {
		case update := <-updates:
			Text := update.Message.Text
			ChatID := update.Message.Chat.ID
			UserID := update.Message.From.UserName

			regexNew, _ := regexp.Compile("^/new (\\D+)$")
			regexAssign, _ := regexp.Compile("^/assign_([0-9]+)$")
			regexUnAssign, _ := regexp.Compile("^/unassign_([0-9]+)$")
			regexResolve, _ := regexp.Compile("^/resolve_([0-9]+)$")

			if Text == "/owner" {

				k := 0

				for _, val := range tasks {
					if val.owner == ChatID {
						k++
					}
				}

				var answer string
				i := 0
				for _, val := range tasks {
					if val.owner == ChatID {
						i++
						if val.assign == ChatID {
							answer += strconv.Itoa(val.id) + ". " + val.text + " by @" + val.ownerUserName + "\n" +
								"/unassign_" + strconv.Itoa(val.id) + " /resolve_" + strconv.Itoa(val.id)
						} else {
							answer += strconv.Itoa(val.id) + ". " + val.text + " by @" + val.ownerUserName + "\n" +
								"/assign_" + strconv.Itoa(val.id)
						}

						if i != k {
							answer += "\n\n"
						}

					}

				}

				msg := tgbotapi.NewMessage(ChatID, answer)
				bot.Send(msg)
			}

			if Text == "/my" {

				var answer string

				k := 0

				for _, val := range tasks {
					if val.assign == ChatID {
						k++
					}
				}

				i := 0
				for _, val := range tasks {
					if val.assign == ChatID {
						i++
						answer += strconv.Itoa(val.id) + ". " + val.text + " by @" + val.ownerUserName + "\n" +
							"/unassign_" + strconv.Itoa(val.id) + " /resolve_" + strconv.Itoa(val.id)

						if i != k {
							answer += "\n\n"
						}
					}

				}

				msg := tgbotapi.NewMessage(ChatID, answer)
				bot.Send(msg)
			}
			if Text == "/tasks" {

				if len(tasks) == 0 {

					msg := tgbotapi.NewMessage(ChatID, "Нет задач")

					//fmt.Println(string(text))
					bot.Send(msg)
				} else {

					var answer string
					for i, val := range tasks {
						answer += strconv.Itoa(val.id) + ". " + val.text + " by @" + val.ownerUserName + "\n"

						if val.assign == 0 { // ни на ком не назначена
							answer += "/assign_" + strconv.Itoa(val.id)
						} else {

							if val.assign == ChatID { // назначена на мне
								answer += "assignee: я\n" +
									"/unassign_" + strconv.Itoa(val.id) + " /resolve_" + strconv.Itoa(val.id)
							} else {
								answer += "assignee: @" + val.assignUserName
							}
						}

						if i+1 != len(tasks) {
							answer += "\n\n"
						}
					}

					msg := tgbotapi.NewMessage(ChatID, answer)
					bot.Send(msg)
				}

			}

			if regexUnAssign.MatchString(Text) { // Отказаться от задачи

				var index int
				commNum, _ := strconv.Atoi(regexUnAssign.FindStringSubmatch(Text)[1])

				for i, val := range tasks {
					if val.id == commNum {
						index = i
						break
					}
				}

				if tasks[index].assign != ChatID { // задача не на вас
					msg := tgbotapi.NewMessage(ChatID, `Задача не на вас`)
					bot.Send(msg)
				} else { // Cнять задачу
					msg1 := tgbotapi.NewMessage(ChatID, `Принято`)

					var text2 string
					text2 = `Задача "` + tasks[index].text + `" осталась без исполнителя`
					msg2 := tgbotapi.NewMessage(tasks[index].owner, text2)

					tasks[index].assign = 0
					tasks[index].assignUserName = ""

					bot.Send(msg1)
					bot.Send(msg2)
				}

			}

			if regexResolve.MatchString(Text) { // Выполнить задачу

				var index int
				commNum, _ := strconv.Atoi(regexResolve.FindStringSubmatch(Text)[1])

				for i, val := range tasks {
					if val.id == commNum {
						index = i
						break
					}
				}

				if tasks[index].assign == ChatID {

					text1 := `Задача "` + tasks[index].text + `" выполнена`
					msg1 := tgbotapi.NewMessage(ChatID, text1)

					text2 := `Задача "` + tasks[index].text + `" выполнена @` + UserID
					msg2 := tgbotapi.NewMessage(tasks[index].owner, text2)

					//a = append(a[:i], a[i+1:]...)
					tasks = append(tasks[:index], tasks[index+1:]...)

					bot.Send(msg1)
					bot.Send(msg2)
				}
			}

			if regexAssign.MatchString(Text) { // Assign

				commNum, _ := strconv.Atoi(regexAssign.FindStringSubmatch(Text)[1])
				//fmt.Println(commNum)
				var index int
				var ID int64

				for i, val := range tasks {
					if val.id == commNum {
						index = i
						break
					}
				}

				text1 := `Задача "` + tasks[index].text + `" назначена на вас`
				text2 := `Задача "` + tasks[index].text + `" назначена на @` + UserID

				msg1 := tgbotapi.NewMessage(ChatID, text1)

				if tasks[index].assign != 0 {
					ID = tasks[index].assign
				} else {
					ID = tasks[index].owner
				}

				msg2 := tgbotapi.NewMessage(ID, text2)

				tasks[index].assign = ChatID
				tasks[index].assignUserName = UserID

				bot.Send(msg1)

				if ChatID != tasks[index].owner {
					bot.Send(msg2)
				}

			}

			if regexNew.MatchString(Text) { // New
				comm := regexNew.FindStringSubmatch(Text)[1]
				//fmt.Println(comm)

				counterTask++
				var task Task

				task.owner = ChatID
				task.id = counterTask
				task.text = comm
				task.assign = 0
				task.assignUserName = ""
				task.ownerUserName = UserID

				tasks = append(tasks, task)

				msg := tgbotapi.NewMessage(ChatID, `Задача "`+comm+`" создана, id=`+strconv.Itoa(counterTask))
				bot.Send(msg)
			}
		}
	}

	return nil
}

func main() {
	err := startTaskBot(context.Background())
	if err != nil {
		panic(err)
	}
}
