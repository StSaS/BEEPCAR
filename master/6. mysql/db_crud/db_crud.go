package main

import (
	"io"
	"io/ioutil"
	"reflect"
	"regexp"
	"strconv"
	//"fmt"
	"database/sql"
	"encoding/json"
	"net/http"
	"strings"
	//"strconv"
	//"encoding/binary"
)


type DB struct { // handler
	tables []string
	rules  map[string]map[string]Rules // [table][field]
	Db     *sql.DB
}

type Rules struct {
	Type    string
	isNull  bool
	Primary bool
}

func (srv *DB) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	regex2, _ := regexp.Compile("^/([a-z_]+)$")
	regexPut, _ := regexp.Compile("^/([a-z]+)/$")
	regex1, _ := regexp.Compile("^/([a-z]+)/([0-9]+)$")

	if r.Method == http.MethodGet {
		if r.URL.Path == "/" { // Возвратить список всех таблиц
			m := make(map[string]map[string]interface{}, 0)
			m["response"] = make(map[string]interface{}, 0)

			//resT, _ := json.Marshal(tables)
			m["response"]["tables"] = srv.tables

			res, _ := json.Marshal(m)
			io.WriteString(w, string(res))

		}

		//regex1, _ := regexp.Compile("^/([a-z]+)/([0-9]+)$")

		if regex1.MatchString(r.URL.Path) { // Возвратить запись по id
			table := regex1.FindStringSubmatch(r.URL.Path)[1]
			id := regex1.FindStringSubmatch(r.URL.Path)[2]
			f := false

			for _, val := range srv.tables {
				if val == table {
					f = true
					break
				}
			}

			if f {
				rows, err := srv.Db.Query("SELECT * FROM "+table+" WHERE id = ?", id)

				//fmt.Println(err)

				if err != nil {

					w.WriteHeader(404)
					m := make(map[string]interface{}, 0)
					m["error"] = "record not found"

					res, _ := json.Marshal(m)
					io.WriteString(w, string(res))

				} else {

					//fmt.Println(err)

					colNames, _ := rows.Columns()

					//fmt.Println(colNames)

					rawResult := make([][]byte, len(colNames))

					cols := make([]interface{}, len(colNames))

					//tables = make([]string,0)

					for i, _ := range rawResult {
						cols[i] = &rawResult[i] // Put pointers to each string in the interface slice
					}

					i := 0
					m := make(map[string]interface{})

					for rows.Next() {

						i++
						err = rows.Scan(cols...)

						for k, raw := range rawResult {

							// проверка на пустоту и если isNULL, то nil
							if (len(raw) == 0) && srv.rules[table][colNames[k]].isNull {
								//fmt.Fprintln(w,nil)
								m[colNames[k]] = nil
								continue
							}

							if strings.Contains(srv.rules[table][colNames[k]].Type, "int") {

								m[colNames[k]] = int(raw[0]) - 48
							} else { //string

								m[colNames[k]] = string(raw)
							}

						}

					}
					if i == 0 {
						w.WriteHeader(404)
						m := make(map[string]interface{}, 0)
						m["error"] = "record not found"

						res, _ := json.Marshal(m)
						io.WriteString(w, string(res))
					} else {
						mm := make(map[string]map[string]interface{}, 0)
						mm["response"] = make(map[string]interface{}, 0)

						//resT, _ := json.Marshal(tables)
						mm["response"]["record"] = m

						res, _ := json.Marshal(mm)
						io.WriteString(w, string(res))
					}
				}
			}

		}

		//regex2, _ := regexp.Compile("^/([a-z]+)$")

		if regex2.MatchString(r.URL.Path) { // Возвратить запись по id
			table := regex2.FindStringSubmatch(r.URL.Path)[1]

			limit, _ := strconv.Atoi(r.URL.Query().Get("limit"))
			offset, _ := strconv.Atoi(r.URL.Query().Get("offset"))
			if r.URL.Query().Get("limit") == "" {
				limit = 5
			}

			//rows, _ := srv.Db.Query("SELECT * FROM "+table+" LIMIT ? OFFSET ?", limit, offset)

			contain := false
			for _, value := range srv.tables {

				if value == table {
					contain = true
					break
				}
			}

			if !contain {

				w.WriteHeader(404)
				m := make(map[string]interface{}, 0)
				m["error"] = "unknown table"

				res, _ := json.Marshal(m)
				io.WriteString(w, string(res))

			} else {

				rows, _ := srv.Db.Query("SELECT * FROM "+table+" LIMIT ? OFFSET ?", limit, offset)

				colNames, _ := rows.Columns()

				rawResult := make([][]byte, len(colNames))

				cols := make([]interface{}, len(colNames))

				for i, _ := range rawResult {
					cols[i] = &rawResult[i] // Put pointers to each string in the interface slice
				}

				sm := make([]map[string]interface{}, 0)

				for rows.Next() {

					err := rows.Scan(cols...)
					err = err

					m := make(map[string]interface{})

					for k, raw := range rawResult {

						if (len(raw) == 0) && srv.rules[table][colNames[k]].isNull {
							//fmt.Fprintln(w,nil)
							m[colNames[k]] = nil
							continue
						}

						if strings.Contains(srv.rules[table][colNames[k]].Type, "int") {

							m[colNames[k]] = int(raw[0]) - 48
						} else { //string

							m[colNames[k]] = string(raw)
						}

					}
					sm = append(sm, m)
				}

				//fmt.Fprintln(w,sm)
				mm := make(map[string]map[string]interface{}, 0)
				mm["response"] = make(map[string]interface{}, 0)

				//resT, _ := json.Marshal(tables)
				mm["response"]["records"] = sm

				res, _ := json.Marshal(mm)
				io.WriteString(w, string(res))

			}
		}

	}

	if (r.Method == http.MethodPut) && (regexPut.MatchString(r.URL.Path)) {
		body, _ := ioutil.ReadAll(r.Body)

		table := regexPut.FindStringSubmatch(r.URL.Path)[1]

		f := false

		for _, val := range srv.tables {
			if val == table {
				f = true
				break
			}
		}

		if f {

			/*b := map[string]interface{}{
				"id":          42, // auto increment primary key игнорируется при вставке
				"title":      "db_crud",
				"description": "",
			}
			body,_ = json.Marshal(b) */

			/*fmt.Fprintln(w,err)
			fmt.Fprintln(w,string(body))*/
			var data map[string]interface{}

			json.Unmarshal(body, &data)
			//fmt.Fprintln(w,data)

			var fields string
			var values string
			var prefix string
			var invalidType string
			var permision bool

			permision = true
			invalidType = ""

			args := []interface{}{}

			prefix = "INSERT INTO " + table
			fields = " ("
			values = " VALUES("
			i := 1

			for k := range data {

				if srv.rules[table][k].Primary { // autoincrement
					i++
					continue
				}

				if (data[k] == nil) && (srv.rules[table][k].isNull) {
					//fmt.Fprintln(w,nil)

					args = append(args, data[k])

					if i == len(data) {
						fields += k + ")"
						values += " ?)"
					} else {
						i++
						fields += k + ", "
						values += " ?,"
					}
					continue
				} else {
					if (data[k] == nil) && (!srv.rules[table][k].isNull) {
						invalidType = k
						permision = false
						break

					}
				}
				//fmt.Fprintln(w,reflect.TypeOf(data[k]).String())
				if (reflect.TypeOf(data[k]).Kind() == reflect.Float64) && (strings.Contains(srv.rules[table][k].Type, "int")) {
					//fmt.Fprintln(w,int(data[k].(float64)))
					args = append(args, int(data[k].(float64)))
				} else {
					if (reflect.TypeOf(data[k]).Kind() == reflect.Float64) && (!strings.Contains(srv.rules[table][k].Type, "int")) {
						invalidType = k
						permision = false
						break
					}
				}

				if (reflect.TypeOf(data[k]).Kind() == reflect.String) && (!strings.Contains(srv.rules[table][k].Type, "int")) {
					//fmt.Fprintln(w,int(data[k].(float64)))
					args = append(args, data[k])
				} else {
					if (reflect.TypeOf(data[k]).Kind() == reflect.String) && (strings.Contains(srv.rules[table][k].Type, "int")) {
						invalidType = k
						permision = false
						break
					}
				}
				//fmt.Fprintln(w,data[k])

				if i == len(data) {
					fields += k + ")"
					values += " ?)"
				} else {
					i++
					fields += k + ", "
					values += " ?,"
				}

			}

			if permision {
				rb, _ := srv.Db.Exec(prefix+fields+values, args...)
				Lid, _ := rb.LastInsertId()

				//fmt.Fprintln(w,rb)
				mm := make(map[string]map[string]interface{}, 0)
				mm["response"] = make(map[string]interface{}, 0)

				//resT, _ := json.Marshal(tables)
				mm["response"]["id"] = Lid

				res, _ := json.Marshal(mm)
				io.WriteString(w, string(res))

				/*	fmt.Fprintln(w,Lid)
					fmt.Fprintln(w,err)*/

			} else {
				w.WriteHeader(400)
				m := make(map[string]interface{}, 0)
				m["error"] = "field " + invalidType + " have invalid type"

				res, _ := json.Marshal(m)
				io.WriteString(w, string(res))
			}

		}

	}

	if (r.Method == http.MethodDelete) && (regex1.MatchString(r.URL.Path)) {

		table := regex1.FindStringSubmatch(r.URL.Path)[1]
		id := regex1.FindStringSubmatch(r.URL.Path)[2]

		f := false

		for _, val := range srv.tables {
			if val == table {
				f = true
				break
			}
		}

		if f {

			result, _ := srv.Db.Exec("DELETE FROM "+table+" WHERE id = ?", id)

			delNum, _ := result.RowsAffected()

			mm := make(map[string]map[string]interface{}, 0)
			mm["response"] = make(map[string]interface{}, 0)

			//resT, _ := json.Marshal(tables)
			mm["response"]["deleted"] = delNum

			res, _ := json.Marshal(mm)
			io.WriteString(w, string(res))
		}

		//fmt.Fprintln(w,delNum)
	}

	// обновлять запись
	if (r.Method == http.MethodPost) && (regex1.MatchString(r.URL.Path)) {
		body, _ := ioutil.ReadAll(r.Body)

		table := regex1.FindStringSubmatch(r.URL.Path)[1]
		id := regex1.FindStringSubmatch(r.URL.Path)[2]

		f := false

		for _, val := range srv.tables {
			if val == table {
				f = true
				break
			}
		}

		if f {
			/*b := map[string]interface{}{
				//"id":          47, // auto increment primary key игнорируется при вставке
				"title":      "crud",
				"description": "good",
				"updated" : nil,
			}
			body,_ = json.Marshal(b) */

			//fmt.Fprintln(w,err)
			//fmt.Fprintln(w,string(body))
			var data map[string]interface{}

			json.Unmarshal(body, &data)
			//fmt.Fprintln(w,data)

			var fields string
			var condition string
			var prefix string
			var invalidType string
			var permision bool

			permision = true
			invalidType = ""

			args := []interface{}{}

			prefix = "UPDATE " + table + " SET "
			fields = ""
			condition = "WHERE id = ?"

			i := 1

			for k := range data {

				if srv.rules[table][k].Primary { // autoincrement
					invalidType = k
					permision = false
					break
				}

				if (data[k] == nil) && (srv.rules[table][k].isNull) {
					//fmt.Fprintln(w,nil)

					args = append(args, data[k])

					if i == len(data) {
						fields += k + " = ? "
					} else {
						i++
						fields += k + " = ?, "
					}
					continue
				} else {
					if (data[k] == nil) && (!srv.rules[table][k].isNull) {
						invalidType = k
						permision = false
						break

					}
				}
				//fmt.Fprintln(w,reflect.TypeOf(data[k]).String())
				if (reflect.TypeOf(data[k]).Kind() == reflect.Float64) && (strings.Contains(srv.rules[table][k].Type, "int")) {
					//fmt.Fprintln(w,int(data[k].(float64)))
					args = append(args, int(data[k].(float64)))
				} else {
					if (reflect.TypeOf(data[k]).Kind() == reflect.Float64) && (!strings.Contains(srv.rules[table][k].Type, "int")) {
						invalidType = k
						permision = false
						break
					}
				}

				if (reflect.TypeOf(data[k]).Kind() == reflect.String) && (!strings.Contains(srv.rules[table][k].Type, "int")) {
					//fmt.Fprintln(w,int(data[k].(float64)))
					args = append(args, data[k])
				} else {
					if (reflect.TypeOf(data[k]).Kind() == reflect.String) && (strings.Contains(srv.rules[table][k].Type, "int")) {
						invalidType = k
						permision = false
						break
					}
				}
				//fmt.Fprintln(w,data[k])

				if i == len(data) {
					fields += k + " = ? "
				} else {
					i++
					fields += k + " = ?, "
				}

			}

			args = append(args, id)

			if permision {
				rb, _ := srv.Db.Exec(prefix+fields+condition, args...)
				updNum, _ := rb.RowsAffected()

				//fmt.Fprintln(w,rb)
				mm := make(map[string]map[string]interface{}, 0)
				mm["response"] = make(map[string]interface{}, 0)

				//resT, _ := json.Marshal(tables)
				mm["response"]["updated"] = updNum

				res, _ := json.Marshal(mm)
				io.WriteString(w, string(res))

			} else {
				w.WriteHeader(400)
				m := make(map[string]interface{}, 0)
				m["error"] = "field " + invalidType + " have invalid type"

				res, _ := json.Marshal(m)
				io.WriteString(w, string(res))
			}

		}
	}

}

func (srv *DB) SetRules(db *sql.DB) {

	for _, table := range srv.tables {
		rows, err := db.Query("SHOW FULL COLUMNS FROM " + table)
		err = err

		//fmt.Println(rows.Columns())
		colNames, _ := rows.Columns()

		rawResult := make([][]byte, len(colNames))

		cols := make([]interface{}, len(colNames))

		for i, _ := range rawResult {
			cols[i] = &rawResult[i] // Put pointers to each string in the interface slice
		}

		srv.rules[table] = make(map[string]Rules)

		for rows.Next() {

			err = rows.Scan(cols...)

			//err = err
			var name string
			var field Rules

			for i, raw := range rawResult {

				if i == 0 { // Name
					name = string(raw)
				}

				if i == 1 { // Type
					field.Type = string(raw)
					//string
					//int

				}

				if i == 3 { // Null
					if string(raw) == "NO" {
						field.isNull = false
					} else {
						field.isNull = true
					}
				}

				if i == 4 { // Key
					if string(raw) == "" {
						field.Primary = false
					} else {
						field.Primary = true
					}
				}

			}

			srv.rules[table][name] = field

		}

	}

}

func (srv *DB) GetAllTables(db *sql.DB) {

	rows, err := db.Query("SHOW TABLES")
	err = err

	colNames, _ := rows.Columns()

	rawResult := make([][]byte, len(colNames))

	cols := make([]interface{}, len(colNames))

	for i, _ := range rawResult {
		cols[i] = &rawResult[i] // Put pointers to each string in the interface slice
	}

	k := 0

	for rows.Next() {

		err = rows.Scan(cols...)

		for _, raw := range rawResult {

			//tables[k] = string(raw)
			srv.tables = append(srv.tables, string(raw))
			//fmt.Print(tables[k])
			k++
			//fmt.Print("	")
		}

		//fmt.Println(len(tables))
	}
}

func NewDbCRUD(db *sql.DB) (http.Handler, error) {
	// ваша реализация тут

	//Db = db
	var database DB = DB{
		Db:     db,
		tables: make([]string, 0),
		rules:  make(map[string]map[string]Rules, 0),
	}
	database.GetAllTables(db)
	database.SetRules(db)

	return &database, nil
}
