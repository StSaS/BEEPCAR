package main

import (
	"fmt"
	"sort"
	"strconv"
	"sync"
	"time"
)

var testResult string
var Mu = &sync.Mutex{}
var Wgc = &sync.WaitGroup{}

type HashSum struct {
	id   int
	Hash string
}

func SingleHash(in, out chan interface{}) {

	wg := &sync.WaitGroup{}

	for i := range in {
		data, _ := i.(int)

		wg.Add(1)

		go func(d int, wg *sync.WaitGroup) {
			defer wg.Done()
			data := d
			Mu.Lock()
			md5 := DataSignerMd5(strconv.Itoa(data))
			Mu.Unlock()

			ch1 := make(chan string)
			var val1, val2 string

			go func() {
				ch1 <- DataSignerCrc32(strconv.Itoa(data))
			}()

			val2 = DataSignerCrc32(md5)

			val1 = <-ch1

			res := new(HashSum)
			res.id = 0 // индекс не важен
			res.Hash = val1 + "~" + val2

			out <- res
		}(data, wg)

	}

	wg.Wait()

}

func MultiHash(in, out chan interface{}) {

	wg := &sync.WaitGroup{}

	for r := range in {
		data, _ := r.(*HashSum)
		wg.Add(1)

		go func(data *HashSum, wg *sync.WaitGroup) {
			defer wg.Done()
			ch := make(chan interface{}, 6)

			for i := 0; i <= 5; i++ {
				go func(i int, in string, out chan<- interface{}) {
					str := DataSignerCrc32(in)

					res := new(HashSum)
					res.id = i
					res.Hash = str

					out <- res
				}(i, strconv.Itoa(i)+data.Hash, ch)
			}

			check := 0
			hashs := make(map[int]string)

			for r := range ch { // агрегация результатов
				data, _ := r.(*HashSum)

				hashs[data.id] = data.Hash

				if check+1 == 6 {
					break
				} else {
					check++
				}
			}

			//склейка
			var result string
			for i := 0; i <= 5; i++ {

				result += hashs[i]
			}

			res := new(HashSum)
			res.id = data.id
			res.Hash = result

			out <- res

		}(data, wg)

	}

	wg.Wait()

}

func CombineResults(in, out chan interface{}) {

	check := 0
	res := make([]string, MaxInputDataLen, 2*MaxInputDataLen)

	for r := range in {
		data, _ := r.(*HashSum)

		res[check] = data.Hash

		check++

	}

	var result string
	agr := res[:check]
	//сортировка результатов по хешу
	sort.Strings(agr)
	result = agr[0]

	for i := 1; i < check; i++ {

		result += "_" + agr[i]
	}

	out <- result
	out <- ""

}

func ExecutePipeline(f ...job) {

	chans := make([]chan interface{}, MaxInputDataLen, 2*MaxInputDataLen)
	count := 0
	chans[count] = make(chan interface{}, MaxInputDataLen)

	for _, F := range f {

		chans[count+1] = make(chan interface{}, MaxInputDataLen)

		Wgc.Add(1)
		go func(in, out *chan interface{}, F job) {
			F(*in, *out) // сh0,ch1 ; ch1,ch2
			close(*out)
			Wgc.Done()
		}(&chans[count], &chans[count+1], F)

		count++ // вызванных функций
	}

	Wgc.Wait()

	return
}

func main() {

	inputData := []int{0, 1, 1, 2, 3, 5, 8}

	hashSignPipeline := []job{
		job(func(in, out chan interface{}) {
			for _, fibNum := range inputData {
				out <- fibNum

			}
		}),
		job(SingleHash),
		job(MultiHash),
		job(CombineResults),
		job(func(in, out chan interface{}) {

			dataRaw := <-in
			<-in
			data, ok := dataRaw.(string)
			if !ok {
				//t.Error("cant convert result data to string")
			}

			testResult = data

		}),
	}

	start := time.Now()

	ExecutePipeline(hashSignPipeline...)

	end := time.Since(start)

	fmt.Println(end)
	fmt.Println(testResult)

}
